/**
 * @constructor
 * @param(Queue) inputQueue
 */
function CanvasMouseListener(inputQueue) {
	
	var self = this;
	$("#canvas").bind( "mouseup", function(e){ self.onMouseUp(e); } );
	
	this.inputQueue = inputQueue;
	
	this.onMouseUp = function(e) {
		var xpos, ypos;
		if(e.offsetX==undefined) // this works for Firefox
		  {
		    xpos = e.pageX-$('#canvas').offset().left;
		    ypos = e.pageY-$('#canvas').offset().top;
		  }             
		  else                     // works in Google Chrome
		  {
		    xpos = e.offsetX;
		    ypos = e.offsetY;
		  }
		this.inputQueue.push(new MouseClickEvent(xpos, ypos));
	};
}

/**
 * @Constructor
 */
function MouseClickEvent(x, y) {
	this.x = x;
	this.y = y;
}

/**
 * @constructor
 * @param capacity
 * @returns {Queue}
 */
function Queue(capacity) {
	
	this.capacity = capacity;
	this.container = new Array();
	this.pos = 0;
	
	/**
	 * @public
	 * @param element
	 */
	this.push = function(element) {
		if(this.pos<this.capacity) {
			this.container[this.pos] = element;
			this.pos++;
			return true;
		}
		return false;
	};
	
	/**
	 * @public
	 */
	this.pop = function() {
		if(this.pos==0) {
			return null;
		}
		var element = this.container[this.pos-1];
		this.pos--;
		return element;
	};
	
	/**
	 * @public
	 * @returns {Boolean}
	 */
	this.isEmpty = function() {
		return this.pos<=0;
	};
	
	/**
	 * @public
	 * @returns {Boolean}
	 */
	this.isFull = function() {
		return this.pos>=this.capacity;
	};
}