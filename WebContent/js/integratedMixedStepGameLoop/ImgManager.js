/**
 * @Constructor
 */
function ImgManager() {
	
	this.ballClockImgs = new Array();
	
	/**
	 * @public
	 * @param(Array) imgLocations - Array of Strings representing the locations of the images to load
	 * @returns an Array of Image objects representing the loaded images
	 */
	this.loadImages = function(imgLocations) {
		for(var i=0; i<imgLocations.length; i++) {
			var image = new Image();
			image.src=imgLocations[i];
			this.ballClockImgs[i] = image;
		}
		return this.ballClockImgs;
	};
};
